# Restaurant Application



## Расположение

- http://localhost:8080/ - главная страница(доступна пользователям со статусами ADMIN и USER)
- http://localhost:8080/admin - главная страница администратора(доступна пользователям со статусом ADMIN)

## Настройки

в application.yaml можно изменить:
- секретный ключ администратора (admin:  secret:  password: 123)
- максимальное число одновременно обрабатыаемых заказов (order: max_simultaneous: 3)
- регулировку времени выполнения заказа (time:   multiplier: 25). При увеличении скорость выполения заказа будет увеличиваться.

## Статусы заказов

- Accepted - принят, но ожидает приготовления
- Cooked - в процессе приготовления
- Completed - приготовление заказа завершилось

## Добавление пользователя

- добавление пользователя со статусом USER - переход по кнопке Sign UP(при входе невошедшего пользователя на доступную ему страницу) и заполнение формы 
- добавление пользователя со статусом ADMIN - перейти на http://localhost:8080/admin/add (доступна пользователям со статусами ADMIN и USER) ввести пароль администратора. При успешном вводе пароля, пользователь будет перенаправлен на страницу регистрации - зарегистрированный а этой странице пользовтаель будет иметь статус ADMIN.

## Функционал администратора

- для добавления блюда находится на страницы редактирования меню и нажать кнопку "Добавить". В форме указать необходимые поля.
- для редактирвоания блюда находится на страницы редактирования меню, выбрать нужную категорию и блюдо и нажать кнопку "Редактирвоать". В форме изменить необходимые поля.
- для просмотра статистик перейти, используя левое боковое меню на странице администратора, на страницу статистик, где ввести необходимые данные.

## Страницы клиента

- главное меню - возможность выбрать из меню блюдо в нужном количестве и добавить в корзину
- корзина - возможности создания заказа, добавления в незавершённый заказ блюд из корзины, оплаты заказа после его завершения
- история заказов - возмоджность просмотра заказов, которые были оплачены


## Особенности реализации

### Обработка заказов

- Пользователь может иметь только один текущий неоплаченный завершённый заказ
- Приоритет обработки заказов определяется временем заказа.
- К моменту запуска сервера не должно быть заказов на стадиях COOKED или ACCEPTED. В случае непредвиденного перезапуска сервера статус данных заказов нужно поменять вручную.

### Используемые паттерны проектирования

- использование OrderServiceProcessDecorator позволяет запускать потоки обработки заказов. При этом оставляет возможность замены декоратора другим, так, чтобы обработка заказов обрабатывалась бы в другом режиме. 

- использование observer в виде SSE позволяет frontend части подписаться на изменение статуса заказа, что убирает необходимость периодического обновления странцы

- использование паттерна цепочка обязанностей позволяет легко изменять порядок приоритета ошибок при регистрации 

