package hse.sotfware.ru.restaurantapp.config;

import hse.sotfware.ru.restaurantapp.repository.DishRepository;
import hse.sotfware.ru.restaurantapp.repository.OrderDishesRepository;
import hse.sotfware.ru.restaurantapp.repository.OrderRepository;
import hse.sotfware.ru.restaurantapp.service.*;
import hse.sotfware.ru.restaurantapp.validator.AlreadyRegisteredUserValidator;
import hse.sotfware.ru.restaurantapp.validator.PasswordConfirmUserValidator;
import hse.sotfware.ru.restaurantapp.validator.UserValidator;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicesConfig {

    @Autowired
    @Qualifier("decorator")
    private OrderServiceProcessDecorator orderServiceProcessDecorator;

    @Bean
    public OrderService orderService() {
        return orderServiceProcessDecorator;
    }
}