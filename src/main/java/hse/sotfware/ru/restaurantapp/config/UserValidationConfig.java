package hse.sotfware.ru.restaurantapp.config;

import hse.sotfware.ru.restaurantapp.validator.AlreadyRegisteredUserValidator;
import hse.sotfware.ru.restaurantapp.validator.PasswordConfirmUserValidator;
import hse.sotfware.ru.restaurantapp.validator.UserValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserValidationConfig {

    @Bean
    public UserValidator userValidator() {
        AlreadyRegisteredUserValidator alreadyRegisteredUserValidator = new AlreadyRegisteredUserValidator();
        alreadyRegisteredUserValidator.setNext(new PasswordConfirmUserValidator());
        return alreadyRegisteredUserValidator;
    }
}
