package hse.sotfware.ru.restaurantapp.controller;

import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import hse.sotfware.ru.restaurantapp.service.DishService;
import hse.sotfware.ru.restaurantapp.validator.AdminPasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.OpAnd;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class AdminController {

    @Autowired
    private AdminPasswordValidator adminPasswordValidator;
    @Autowired
    private DishService dishService;

    @GetMapping("/admin/add")
    public String addAdminPasswordShow(Model model) {
        model.addAttribute("error_message", "");
        return "admin-password";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/admin")
    public String adminPanel(Model model) {
        return "redirect:/admin/edit/menu";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/admin/edit/menu")
    public String adminPanelEditMenu(Model model) {
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        return "admin-panel-edit-menu";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/admin/statistics")
    public String adminPanelStatistics(@RequestParam("popularity_dish_list_limit") Optional<Integer> popularityDishListLimit, Model model) {
        model.addAttribute("most_popular_dishes", dishService.getMostPopularDishes(popularityDishListLimit.orElse(10)));
        return "admin-panel-statistics";
    }

    @PostMapping("/admin/add/registration")
    public String addAdmin(@RequestParam String password, Model model) {
        if (!adminPasswordValidator.validate(password)) {
            model.addAttribute("error_message", "Incorrect password");
            return "admin-password";
        }
        model.addAttribute("role_input", "ROLE_ADMIN");
        model.addAttribute("user_registration_details", new UserRegistrationDetails());
        return "registration";
    }
}
