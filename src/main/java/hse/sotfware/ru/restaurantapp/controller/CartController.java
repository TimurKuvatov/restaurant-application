package hse.sotfware.ru.restaurantapp.controller;

import hse.sotfware.ru.restaurantapp.config.UserCustomDetails;
import hse.sotfware.ru.restaurantapp.service.CartService;
import hse.sotfware.ru.restaurantapp.service.OrderServiceImpl;
import hse.sotfware.ru.restaurantapp.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CartController {
    @Autowired
    private CartService cartService;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/cart/add/{dishId}")
    public String addDishToCart(@PathVariable("dishId") Long dishId, Integer count, @AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        cartService.addDishToCart(userDetails.getUserId(), dishId, count);
        return "redirect:/cart";
    }

    @PostMapping("/cart/remove/{dishId}")
    public String removeDishFromCart(@PathVariable("dishId") Long dishId, @AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        cartService.removeDishFromCart(userDetails.getUserId(), dishId);
        return "redirect:/cart";
    }

    @PostMapping("/cart/change/{dishId}")
    public String changeDishCountCart(@PathVariable("dishId") Long dishId, @RequestParam int count, @AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        cartService.changeDishCount(userDetails.getUserId(), dishId, count);
        return "redirect:/cart";
    }


    @GetMapping("/cart")
    public String showCart(Model model, @AuthenticationPrincipal UserCustomDetails userDetails) {
        CartUtils.addAttributesToCartModel(model, userDetails.getUserId(), cartService, orderService, paymentService);
        return "cart";
    }
}
