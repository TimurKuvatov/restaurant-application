package hse.sotfware.ru.restaurantapp.controller;

import hse.sotfware.ru.restaurantapp.model.CartItem;
import hse.sotfware.ru.restaurantapp.model.OrderItem;
import hse.sotfware.ru.restaurantapp.service.CartService;
import hse.sotfware.ru.restaurantapp.service.OrderService;
import hse.sotfware.ru.restaurantapp.service.OrderServiceImpl;
import hse.sotfware.ru.restaurantapp.service.PaymentService;
import org.springframework.ui.Model;

import java.util.Comparator;
import java.util.List;

public class CartUtils {

    public static void addAttributesToCartModel(Model model, Long userId, CartService cartService, OrderService orderService, PaymentService paymentService) {
        List<CartItem> cartItemList = cartService.findDishesByUserId(userId);
        model.addAttribute("cart_items", cartItemList.stream().sorted((Comparator.comparing(itemLft -> itemLft.getDish().getName()))).toList());
        OrderItem orderItem = orderService.findOrderItemByUserId(userId);
        model.addAttribute("order_item", orderItem);
        model.addAttribute("total_price", paymentService.calculateTotalPrice(orderItem));
        if (cartItemList != null) {
            model.addAttribute("cart_total_price", paymentService.calculateTotalPrice(cartItemList));
        }
    }
}