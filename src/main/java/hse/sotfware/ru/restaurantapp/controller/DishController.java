package hse.sotfware.ru.restaurantapp.controller;

import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.DishType;
import hse.sotfware.ru.restaurantapp.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DishController {

    @Autowired
    private DishService dishService;


    @GetMapping("/main_menu")
    public String mainMenu(Model model) {
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        return "main-menu";
    }

    @GetMapping("/main_menu/{dishType}")
    public String showUserDishType(@PathVariable(value = "dishType") DishType dishType, Model model) {
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        model.addAttribute("dishes", dishService.findDishesByDishType(dishType));
        return "main-menu";
    }


    @GetMapping("/admin/dish/{dishType}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String showAdminDishType(@PathVariable(value = "dishType") DishType dishType, Model model) {
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        model.addAttribute("dishes", dishService.findDishesByDishType(dishType));
        return "admin-panel-edit-menu";
    }

    @GetMapping("/admin/dish/add")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getAddDishForm(Model model) {
        model.addAttribute("dish", new Dish());
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        model.addAttribute("operation_type", "Create");
        return "admin-dish-form";
    }

    @PostMapping("/admin/dish/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String saveDish(Dish dish, Model model) {
        dishService.saveDish(dish);
        return "redirect:/admin";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/admin/dish/edit")
    public String getEditDishForm(Long editedDishId, Model model) {
        model.addAttribute("dish", dishService.findDishById(editedDishId));
        model.addAttribute("dish_types", dishService.findAllDishTypes());
        model.addAttribute("operation_type", "Edit");
        return "admin-dish-form";
    }
}
