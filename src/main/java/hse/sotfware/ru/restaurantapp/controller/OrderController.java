package hse.sotfware.ru.restaurantapp.controller;

import hse.sotfware.ru.restaurantapp.config.UserCustomDetails;
import hse.sotfware.ru.restaurantapp.model.OrderItem;
import hse.sotfware.ru.restaurantapp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderNotifierService orderNotifierService;


    @Autowired
    private PaymentService paymentService;


    @PostMapping("/order/create")
    public String createOrder(@AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        StringBuffer message = new StringBuffer();
        if (!orderService.tryCreateOrder(message, userDetails.getUserId())) {
            model.addAttribute("error_message", message.toString());
        }
        CartUtils.addAttributesToCartModel(model, userDetails.getUserId(), cartService, orderService, paymentService);
        return "cart";
    }

    @PostMapping("/order/add")
    public String addToOrder(@AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        StringBuffer message = new StringBuffer();
        if (!orderService.tryAddToOrderOrder(message, userDetails.getUserId())) {
            model.addAttribute("error_message", message.toString());
        }
        CartUtils.addAttributesToCartModel(model, userDetails.getUserId(), cartService, orderService, paymentService);
        return "cart";
    }

    @PostMapping("/order/cancel")
    public String cancelOrder(@AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        orderService.cancelOrder(userDetails.getUserId());
        CartUtils.addAttributesToCartModel(model, userDetails.getUserId(), cartService, orderService, paymentService);
        return "cart";
    }

    @PostMapping("/order/pay")
    public String payForOrder(@AuthenticationPrincipal UserCustomDetails userDetails, Model model) {
        paymentService.payForOrder(userDetails.getUserId());
        CartUtils.addAttributesToCartModel(model, userDetails.getUserId(), cartService, orderService, paymentService);
        return "cart";
    }

    @GetMapping("/order/status")
    public SseEmitter getOrderStatusEmitter(@AuthenticationPrincipal UserCustomDetails userDetails) {
        return orderNotifierService.getEmitter(userDetails.getUserId());
    }

    @GetMapping("/order/history")
    public String getOrderPaidForHistory(Model model, @AuthenticationPrincipal UserCustomDetails userDetails) {
        List<OrderItem> orderItems = orderService.findOrderHistory(userDetails.getUserId());
        model.addAttribute("order_items", orderItems);
        model.addAttribute("total_prices", orderItems.stream().map(orderItem -> paymentService.calculateTotalPrice(orderItem)).toList());
        return "paid-for-orders";
    }
}
