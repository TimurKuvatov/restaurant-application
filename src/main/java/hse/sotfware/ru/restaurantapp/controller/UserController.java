package hse.sotfware.ru.restaurantapp.controller;


import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import hse.sotfware.ru.restaurantapp.service.UserService;
import hse.sotfware.ru.restaurantapp.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/registration")
    public String showRegistrationForm(Model model) {
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails();
        userRegistrationDetails.setRole("ROLE_USER");
        model.addAttribute("user_registration_details", userRegistrationDetails);
        model.addAttribute("error_message", "");
        return "registration";
    }

    @PostMapping("/registration/attempt")
    public String registration(UserRegistrationDetails userRegistrationDetails, String savedRole, Model model) {
        userRegistrationDetails.setRole(savedRole);
        StringBuffer message = new StringBuffer();
        if (!userValidator.validate(userRegistrationDetails, message)) {
            model.addAttribute("error_message", message.toString());
            model.addAttribute("user_registration_details", new UserRegistrationDetails());
            return "registration";
        }
        userService.save(userRegistrationDetails);
        return switch (userRegistrationDetails.getRole()) {
            case "ROLE_USER" -> "redirect:/main_menu";
            case "ROLE_ADMIN" -> "redirect:/admin";
            default -> "redirect:/main_menu";
        };
    }

    @GetMapping({"/", "/welcome"})
    public String welcome(Model model) {
        return "redirect:main_menu";
    }
}
