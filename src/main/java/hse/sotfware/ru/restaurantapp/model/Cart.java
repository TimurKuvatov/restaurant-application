package hse.sotfware.ru.restaurantapp.model;


import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;


@Data
@Entity
@Table(name = "cart")
@IdClass(CartId.class)
public class Cart {
    @Id
    @Column(name = "user_id")
    private Long userId;

    @Id
    @Column(name = "dish_id")
    private Long dishId;

    @Column(name = "count")
    private Integer count;
}
