package hse.sotfware.ru.restaurantapp.model;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class CartId implements Serializable {
    private Long userId;
    private Long dishId;

}
