package hse.sotfware.ru.restaurantapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class CartItem {
    private Dish dish;
    private Integer count;

    public CartItem() {
    }

    public CartItem(Dish dish, Integer count) {
        this.dish = dish;
        this.count = count;
    }

}
