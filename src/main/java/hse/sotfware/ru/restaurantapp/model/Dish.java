package hse.sotfware.ru.restaurantapp.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "dish")
@NoArgsConstructor
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, name = "name")
    private String name;
    @Column(name = "number")
    private int number;
    @Column(name = "dish_type")
    @Enumerated(EnumType.STRING)
    private DishType dishType;
    @Column(name = "minutes_for_cooking")
    private int minutesForCooking;
    @Column(name = "price")
    private int price;

    public Dish(int minutesForCooking, int number, int price, Long id, DishType dishType, String name) {
        this.minutesForCooking = minutesForCooking;
        this.number = number;
        this.price = price;
        this.id = id;
        this.dishType = dishType;
        this.name = name;
    }
}
