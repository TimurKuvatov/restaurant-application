package hse.sotfware.ru.restaurantapp.model;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

public enum DishType {

    STARTER("Закуски", "STARTER"),
    SALAD("Салаты", "SALAD"),
    SOUP("Супы", "SOUP"),
    MAIN_COURSE("Основные блюда", "MAIN_COURSE"),
    DRINK("Напитки", "DRINK");

    @Getter
    private final String nameToString;
    private final String stringNameCopy;

    public String getNameToRestore() {
        return stringNameCopy;
    }

    public static DishType ConvertFromReadableFormToDishType(String name) {
        return Arrays.stream(DishType.values()).filter(dishType -> Objects.equals(dishType.getNameToString(), name)).findFirst().orElse(null);
    }

    public static DishType ConvertFromStringNameToDishType(String name) {
        return Arrays.stream(DishType.values()).filter(dishType -> Objects.equals(dishType.getNameToRestore(), name)).findFirst().orElse(null);
    }

    DishType(String stringName, String stringNameCopy) {
        nameToString = stringName;
        this.stringNameCopy = stringNameCopy;
    }
}
