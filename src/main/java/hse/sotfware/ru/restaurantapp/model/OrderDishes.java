package hse.sotfware.ru.restaurantapp.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@IdClass(OrderDishesId.class)
@Table(name = "order_dishes")
@AllArgsConstructor
@NoArgsConstructor
public class OrderDishes {
    @Id
    @Column(name = "order_id")
    private Long orderId;

    @Id
    @Column(name = "dish_id")
    private Long dishId;

    private Integer count;
}
