package hse.sotfware.ru.restaurantapp.model;

import jakarta.persistence.Embeddable;
import lombok.Data;

import java.io.Serializable;

@Data
@Embeddable
public class OrderDishesId implements Serializable {
    private Long orderId;
    private Long dishId;
}
