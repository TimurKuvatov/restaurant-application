package hse.sotfware.ru.restaurantapp.model;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class OrderProcessingTask implements Runnable {
    private final AtomicLong processingTime;
    @Getter
    private final Order order;

    private static final long DELTA_TIME = 1;
    private static final long MILLISECONDS_COUNT = 1000;

    @Value("${time.multiplier}")
    private static final long TIME_MULTIPLIER = 4000;

    private volatile boolean cancel;


    public Long getOrderId() {
        return order.getId();
    }

    public OrderProcessingTask(long processingTime, Order order) {
        this.processingTime = new AtomicLong(processingTime);
        this.order = order;
    }

    public void addProcessingTime(long additionalProcessingTime) {
        processingTime.set(processingTime.longValue() + additionalProcessingTime);
    }


    @Override
    public void run() {
        long current_time = 0;
        try {
            while (!cancel && current_time < processingTime.longValue() * TIME_MULTIPLIER) {
                TimeUnit.MILLISECONDS.sleep(DELTA_TIME * MILLISECONDS_COUNT);
                current_time += DELTA_TIME * TIME_MULTIPLIER;
            }
        } catch (InterruptedException ignored) {
        }
    }

    public void cancel() {
        cancel = true;
    }
}
