package hse.sotfware.ru.restaurantapp.model;

import java.util.Arrays;
import java.util.Objects;

public enum OrderStatus {
    ACCEPTED("Принят", "ACCEPTED"),
    COOKED("Готовится", "COOKED"),

    CANCELED("Отменён", "CANCELED"),

    PAID_FOR("Оплачен", "PAID_FOR"),
    COMPLETED("Готов", "COMPLETED");

    private final String stringStatus;
    private final String stringCopyEnum;

    public String getStringNameStatus() {
        return stringStatus;
    }

    public static OrderStatus ConvertFromStringStatusToEnum(String status) {
        return Arrays.stream(OrderStatus.values()).filter(orderStatus -> Objects.equals(orderStatus.getStringNameStatus(), status)).findFirst().orElse(null);
    }

    OrderStatus(String stringName, String stringCopyEnum) {
        stringStatus = stringName;
        this.stringCopyEnum = stringCopyEnum;
    }
}
