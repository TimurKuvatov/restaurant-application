package hse.sotfware.ru.restaurantapp.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Data
@Builder
public class Subscriber {
    private Long userId;
    private SseEmitter emitter;
}
