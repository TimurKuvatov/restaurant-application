package hse.sotfware.ru.restaurantapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
public class UserRegistrationDetails {
    private String username;
    private String password;
    private String passwordConfirmation;
    private String role;
}

