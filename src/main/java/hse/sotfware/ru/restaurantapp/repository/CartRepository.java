package hse.sotfware.ru.restaurantapp.repository;

import hse.sotfware.ru.restaurantapp.model.Cart;
import hse.sotfware.ru.restaurantapp.model.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(value = "select d.minutes_for_cooking,d.number,d.price,d.id,d.dish_type,d.name,c.count from cart c left join dish d on (c.dish_id=d.id) where c.user_id = :userId", nativeQuery = true)
    List<Object[]> findDishesByUserId(@Param("userId") Long userId);

    Optional<Cart> findCartByUserIdAndDishId(Long userId, Long dishId);

    void deleteAllByUserId(Long userId);

    @Transactional
    void deleteCartByUserIdAndDishId(Long userId, Long dishId);
}
