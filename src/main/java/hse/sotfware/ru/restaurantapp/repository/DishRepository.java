package hse.sotfware.ru.restaurantapp.repository;

import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.DishType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DishRepository extends JpaRepository<Dish, Long> {
    Dish findDishByName(String name);

    void deleteDishByName(String name);

    Dish findDishById(Long id);

    List<Dish> findDishesByDishType(DishType dishType);

    @Query(value = "SELECT d.minutes_for_cooking,d.number,d.price,d.id,d.dish_type,d.name FROM dish d JOIN order_dishes od ON d.id = od.dish_id GROUP BY d.id ORDER BY COUNT(od) DESC FETCH FIRST :limit ROWS ONLY", nativeQuery = true)
    List<Object[]> findDishesOrderByPopularityLimit(@Param("limit") int limit);
}
