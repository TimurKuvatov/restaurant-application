package hse.sotfware.ru.restaurantapp.repository;

import hse.sotfware.ru.restaurantapp.model.OrderDishes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderDishesRepository extends JpaRepository<OrderDishes, Long> {
    List<OrderDishes> findOrderDishesByOrderId(Long orderId);

    Optional<OrderDishes> findOrderDishesByOrderIdAndDishId(Long orderId, Long dishId);
}
