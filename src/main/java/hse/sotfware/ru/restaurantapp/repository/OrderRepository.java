package hse.sotfware.ru.restaurantapp.repository;

import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.Order;
import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findOrderById(Long orderId);

    Optional<Order> findOrderByUserIdAndStatusIsNotIn(Long userId, List<OrderStatus> status);

    Optional<List<Order>> findOrderByUserIdAndStatus(Long userId, OrderStatus orderStatus);

}
