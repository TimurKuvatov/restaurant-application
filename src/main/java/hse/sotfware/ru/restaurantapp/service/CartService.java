package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.*;
import hse.sotfware.ru.restaurantapp.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public interface CartService {

    void addDishToCart(Long userId, Long dishId, Integer count);

    void removeDishFromCart(Long userId, Long dishId);

    List<CartItem> findDishesByUserId(Long userId);

    void deleteAllByUserId(Long userId);

    void changeDishCount(Long userId, Long dishId, int count);
}
