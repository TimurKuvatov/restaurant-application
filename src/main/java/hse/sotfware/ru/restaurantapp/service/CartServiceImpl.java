package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.Cart;
import hse.sotfware.ru.restaurantapp.model.CartItem;
import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.DishType;
import hse.sotfware.ru.restaurantapp.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    public void addDishToCart(Long userId, Long dishId, Integer count) {
        Cart cart = cartRepository.findCartByUserIdAndDishId(userId, dishId).orElse(new Cart());
        cart.setUserId(userId);
        cart.setDishId(dishId);
        cart.setCount(count);
        cartRepository.save(cart);
    }

    public void removeDishFromCart(Long userId, Long dishId) {
        cartRepository.deleteCartByUserIdAndDishId(userId, dishId);
    }

    public List<CartItem> findDishesByUserId(Long userId) {
        return cartRepository.findDishesByUserId(userId).stream()
                .map(cartItem -> CartItem.builder().dish(new Dish((int) cartItem[0], (int) cartItem[1], (int) cartItem[2], (Long) cartItem[3], DishType.ConvertFromStringNameToDishType((String) cartItem[4]), (String) cartItem[5])).count((int) cartItem[6]).build())
                .collect(Collectors.toList());
    }

    public void deleteAllByUserId(Long userId) {
        cartRepository.deleteAllByUserId(userId);
    }

    public void changeDishCount(Long userId, Long dishId, int count) {
        Optional<Cart> cartItem = cartRepository.findCartByUserIdAndDishId(userId, dishId);
        if (cartItem.isPresent()) {
            cartItem.get().setCount(count);
            cartRepository.save(cartItem.get());
        }
    }
}
