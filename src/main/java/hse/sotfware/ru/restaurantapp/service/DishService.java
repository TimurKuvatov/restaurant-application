package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.DishType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface DishService {

    List<Dish> findAllDishes();

    DishType[] findAllDishTypes();


    Dish findDishByName(String name);

    Dish findDishById(Long id);

    List<Dish> findDishesByDishType(DishType dishType);

    void deleteDishByName(String name);

    void saveDish(Dish dish);

    void updateDish(Dish dish);

    List<Dish> getMostPopularDishes(int limit);
}
