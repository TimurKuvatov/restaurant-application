package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.CartItem;
import hse.sotfware.ru.restaurantapp.model.Dish;
import hse.sotfware.ru.restaurantapp.model.DishType;
import hse.sotfware.ru.restaurantapp.repository.DishRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DishServiceImpl implements DishService {
    private DishRepository dishRepository;

    @Override
    public List<Dish> findAllDishes() {
        return dishRepository.findAll();
    }

    @Override
    public DishType[] findAllDishTypes() {
        return DishType.values();
    }


    @Override
    public Dish findDishByName(String name) {
        return dishRepository.findDishByName(name);
    }

    @Override
    public List<Dish> findDishesByDishType(DishType dishType) {
        return dishRepository.findDishesByDishType(dishType);
    }

    @Override
    public Dish findDishById(Long id) {
        return dishRepository.findDishById(id);
    }


    @Override
    public void deleteDishByName(String name) {
        dishRepository.deleteDishByName(name);
    }

    @Override
    public void saveDish(Dish dish) {

        dishRepository.save(dish);
    }

    @Override
    public void updateDish(Dish dish) {
        dishRepository.save(dish);
    }

    public List<Dish> getMostPopularDishes(int limit) {
        return dishRepository.findDishesOrderByPopularityLimit(limit).stream()
                .map(dish -> new Dish((int) dish[0], (int) dish[1], (int) dish[2], (Long) dish[3], DishType.ConvertFromStringNameToDishType((String) dish[4]), (String) dish[5]))
                .collect(Collectors.toList());
    }
}
