package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import hse.sotfware.ru.restaurantapp.model.Subscriber;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class OrderNotifierService {
    private final List<Subscriber> subscribers = new CopyOnWriteArrayList<>();

    private Optional<Subscriber> findSubscriberByUserId(Long userId) {
        return subscribers.stream().filter(subscriber -> Objects.equals(subscriber.getUserId(), userId)).findFirst();
    }

    public SseEmitter getEmitter(Long userId) {
        synchronized (subscribers) {
            Optional<Subscriber> subscriber = findSubscriberByUserId(userId);
            if (subscriber.isPresent()) {
                return subscriber.get().getEmitter();
            }
            SseEmitter emitter = new SseEmitter(0L);
            subscribers.add(Subscriber.builder().emitter(emitter).userId(userId).build());
            return emitter;
        }
    }

    public void sendOrderStatusUpdate(OrderStatus orderStatus, Long userId) {
        synchronized (subscribers) {
            subscribers.forEach(subscriber -> {
                if (Objects.equals(subscriber.getUserId(), userId)) {
                    try {
                        subscriber.getEmitter().send(orderStatus.name());
                        if (orderStatus == OrderStatus.COMPLETED) {
                            subscribers.remove(subscriber);
                        }
                    } catch (IOException | RequestRejectedException | IllegalStateException ignored) {
                    }
                }
            });
        }
    }
}