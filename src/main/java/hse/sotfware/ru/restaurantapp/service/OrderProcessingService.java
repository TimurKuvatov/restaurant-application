package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.Order;
import hse.sotfware.ru.restaurantapp.model.OrderProcessingTask;
import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.concurrent.*;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class OrderProcessingService implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Value("${order.max_simultaneous}")
    private int maxSimultaneousOrdersCount = 10;
    private final BlockingQueue<Runnable> orderQueue = new LinkedBlockingQueue<>();
    private final HashSet<Runnable> currentActiveThreads = new HashSet<>();
    private final BlockingQueue<Runnable> orderWaitQueue = new LinkedBlockingQueue<>();

    private final ThreadPoolExecutor orderExecutor = new ThreadPoolExecutor(maxSimultaneousOrdersCount, maxSimultaneousOrdersCount * 100,
            0L, TimeUnit.MILLISECONDS, orderQueue);

    private void execute(OrderProcessingTask orderProcessingTask) {
        applicationEventPublisher.publishEvent(new OrderStatusUpdateEvent(this, OrderStatus.COOKED, orderProcessingTask.getOrder().getUserId(), orderProcessingTask.getOrder().getId()));
        FutureTask<Void> futureTask = new FutureTask<>(orderProcessingTask, null);
        orderExecutor.submit(futureTask);
        synchronized (currentActiveThreads) {
            currentActiveThreads.add(orderProcessingTask);
        }
        orderExecutor.execute(() -> {
            try {
                futureTask.get();
                applicationEventPublisher.publishEvent(new OrderStatusUpdateEvent(this, OrderStatus.COMPLETED, orderProcessingTask.getOrder().getUserId(), orderProcessingTask.getOrder().getId()));
                synchronized (currentActiveThreads) {
                    currentActiveThreads.remove(orderProcessingTask);
                }
                synchronized (orderWaitQueue) {
                    if (!orderWaitQueue.isEmpty()) {
                        Runnable orderTask = orderWaitQueue.peek();
                        orderWaitQueue.remove(orderTask);
                        execute((OrderProcessingTask) Objects.requireNonNull(orderTask));
                    }
                }
            } catch (InterruptedException | ExecutionException ignored) {
            }
        });
    }

    public void addOrder(Order order, long processingTime) {
        OrderProcessingTask orderProcessingTask = new OrderProcessingTask(processingTime, order);
        if (orderExecutor.getActiveCount() < maxSimultaneousOrdersCount) {
            execute(orderProcessingTask);
        } else {
            synchronized (orderWaitQueue) {
                orderWaitQueue.add(orderProcessingTask);
                orderWaitQueue.notify();
            }
        }
    }

    public void addProcessingTime(long additionProcessingTime, Long orderId) {
        synchronized (orderWaitQueue) {
            orderWaitQueue.forEach(orderProcessingTask -> {
                if (orderProcessingTask instanceof OrderProcessingTask && Objects.equals(((OrderProcessingTask) orderProcessingTask).getOrderId(), orderId)) {
                    ((OrderProcessingTask) orderProcessingTask).addProcessingTime(additionProcessingTime);
                }
            });
        }
        synchronized (currentActiveThreads) {
            currentActiveThreads.forEach(orderProcessingTask -> {
                if (orderProcessingTask instanceof OrderProcessingTask && Objects.equals(((OrderProcessingTask) orderProcessingTask).getOrderId(), orderId)) {
                    ((OrderProcessingTask) orderProcessingTask).addProcessingTime(additionProcessingTime);
                }
            });
        }
    }

    public void cancelOrder(Long orderId) {
        synchronized (currentActiveThreads) {
            currentActiveThreads.forEach(orderProcessingTask -> {
                if (orderProcessingTask instanceof OrderProcessingTask && Objects.equals(((OrderProcessingTask) orderProcessingTask).getOrderId(), orderId)) {
                    ((OrderProcessingTask) orderProcessingTask).cancel();
                }
            });
        }
    }

    @Override
    public void setApplicationEventPublisher(@NonNull ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}