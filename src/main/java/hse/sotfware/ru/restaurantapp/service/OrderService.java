package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.*;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;


public interface OrderService {

    @Transactional
    boolean tryCreateOrder(StringBuffer message, Long userId);

    @Transactional
    boolean tryAddToOrderOrder(StringBuffer message, Long userId);

    OrderItem findOrderItemByUserId(Long userId);

    long getProcessingTime(Long orderId);

    void updateOrderStatus(Long orderId, OrderStatus orderStatus);

    Optional<Order> cancelOrder(Long userId);

    void changeOrderStatus(Long userId, OrderStatus prevStatus, OrderStatus currentStatus);

    List<OrderItem> findOrderHistory(Long userId);

}
