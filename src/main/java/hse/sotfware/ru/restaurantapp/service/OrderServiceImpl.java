package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.*;
import hse.sotfware.ru.restaurantapp.repository.DishRepository;
import hse.sotfware.ru.restaurantapp.repository.OrderDishesRepository;
import hse.sotfware.ru.restaurantapp.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Service
@AllArgsConstructor
@Qualifier("base_impl")
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private DishRepository dishRepository;
    private OrderDishesRepository orderDishesRepository;
    private CartService cartService;

    @Transactional
    public boolean tryCreateOrder(StringBuffer message, Long userId) {
        if (orderRepository.findOrderByUserIdAndStatusIsNotIn(userId, new ArrayList<>(List.of(OrderStatus.PAID_FOR, OrderStatus.CANCELED))).isPresent()) {
            message.append("У Вас уже есть текущий заказ.");
            return false;
        }
        Order order = new Order();
        order.setUserId(userId);
        order.setStatus(OrderStatus.ACCEPTED);
        return updateOrCreateOrderOrder(message, userId, order);
    }

    @Transactional
    public boolean tryAddToOrderOrder(StringBuffer message, Long userId) {
        Optional<Order> orderOptional = orderRepository.findOrderByUserIdAndStatusIsNotIn(userId, new ArrayList<>(List.of(OrderStatus.PAID_FOR, OrderStatus.CANCELED)));
        if (orderOptional.isEmpty()) {
            message.append("Не удалось найти текущих заказов");
            return false;
        }
        if (orderOptional.get().getStatus() == OrderStatus.COMPLETED) {
            message.append("Нельзя добавить блюда в завершённый заказ");
            return false;
        }
        return updateOrCreateOrderOrder(message, userId, orderOptional.get());
    }

    @Transactional
    public boolean updateOrCreateOrderOrder(StringBuffer message, Long userId, Order order) {
        List<CartItem> cartItems = cartService.findDishesByUserId(userId);
        for (CartItem cartItem : cartItems) {
            int maxCount = dishRepository.findDishById(cartItem.getDish().getId()).getNumber();
            if (maxCount < cartItem.getCount()) {
                message.append("Количество доступных позиций ").append(cartItem.getDish().getName()).append(" - ").append(maxCount);
                return false;
            }
        }
        order.setLastStatusUpdate(LocalDateTime.now());
        orderRepository.save(order);
        for (CartItem cartItem : cartItems) {
            Optional<OrderDishes> orderDishesOptional = orderDishesRepository.findOrderDishesByOrderIdAndDishId(order.getId(), cartItem.getDish().getId());
            orderDishesRepository.save(new OrderDishes(order.getId(), cartItem.getDish().getId(), orderDishesOptional.map(orderDishes -> orderDishes.getCount() + cartItem.getCount()).orElseGet(cartItem::getCount)));
            cartItem.getDish().setNumber(cartItem.getDish().getNumber() - cartItem.getCount());
            dishRepository.save(cartItem.getDish());
        }
        cartService.deleteAllByUserId(userId);
        return true;
    }

    public OrderItem findOrderItemByUserId(Long userId) {
        OrderItem orderItem = new OrderItem();
        Optional<Order> order = orderRepository.findOrderByUserIdAndStatusIsNotIn(userId, new ArrayList<>(List.of(OrderStatus.PAID_FOR, OrderStatus.CANCELED)));
        if (order.isPresent()) {
            orderItem.setOrder(order.get());
        } else {
            return null;
        }
        List<CartItem> cartItems = new ArrayList<>();
        List<OrderDishes> orderDishes = orderDishesRepository.findOrderDishesByOrderId(order.get().getId());
        orderDishes.forEach(orDishes -> cartItems.add(CartItem.builder().dish(dishRepository.findDishById(orDishes.getDishId())).count(orDishes.getCount()).build()));
        orderItem.setCartItems(cartItems);
        return orderItem;
    }

    public List<OrderItem> findOrderHistory(Long userId) {
        Optional<List<Order>> orderOptionalList = orderRepository.findOrderByUserIdAndStatus(userId, OrderStatus.PAID_FOR);
        return orderOptionalList.map(orders -> orders.stream().map(order -> new OrderItem(order,
                orderDishesRepository.findOrderDishesByOrderId(order.getId()).stream().map(
                        orderDishes -> CartItem.builder().dish(dishRepository.findDishById(orderDishes.getDishId())).count(orderDishes.getCount()).build()).toList()
        )).toList()).orElse(Collections.emptyList());
    }

    public long getProcessingTime(Long orderId) {
        Optional<Order> order = orderRepository.findOrderById(orderId);
        if (order.isEmpty()) {
            return 0;
        }
        OrderItem orderItem = findOrderItemByUserId(order.get().getUserId());
        AtomicLong maxTime = new AtomicLong();
        AtomicLong sumTime = new AtomicLong();
        orderItem.getCartItems().forEach(cartItem -> {
            long timeForCookingDishes = (long) (cartItem.getDish().getMinutesForCooking() * (1 + cartItem.getCount() * 1.0 / 10));
            maxTime.set(Math.max(maxTime.get(), timeForCookingDishes));
            sumTime.set(sumTime.get() * timeForCookingDishes);
        });
        return (long) (maxTime.get() * (1 + sumTime.get() * 1.0 / (sumTime.get() - maxTime.get())));
    }

    public void updateOrderStatus(Long orderId, OrderStatus orderStatus) {
        Optional<Order> order = orderRepository.findOrderById(orderId);
        if (order.isPresent() && order.get().getStatus() != OrderStatus.CANCELED) {
            order.get().setLastStatusUpdate(LocalDateTime.now());
            order.get().setStatus(orderStatus);
            orderRepository.save(order.get());
        }
    }

    public Optional<Order> cancelOrder(Long userId) {
        Optional<Order> orderOptional = orderRepository.findOrderByUserIdAndStatusIsNotIn(userId, new ArrayList<>(List.of(OrderStatus.PAID_FOR, OrderStatus.CANCELED)));
        orderOptional.ifPresent(order -> updateOrderStatus(order.getId(), OrderStatus.CANCELED));
        return orderOptional;
    }

    public void changeOrderStatus(Long userId, OrderStatus prevStatus, OrderStatus currentStatus) {
        Optional<List<Order>> orderListOptional = orderRepository.findOrderByUserIdAndStatus(userId, prevStatus);
        if (orderListOptional.isPresent()) {
            Order order = orderListOptional.get().get(orderListOptional.get().size() - 1);
            order.setLastStatusUpdate(LocalDateTime.now());
            order.setStatus(currentStatus);
            orderRepository.save(order);
        }
    }
}
