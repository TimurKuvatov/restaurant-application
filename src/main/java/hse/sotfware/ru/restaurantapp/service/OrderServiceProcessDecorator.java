package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.Order;
import hse.sotfware.ru.restaurantapp.model.OrderItem;
import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.security.config.annotation.web.oauth2.login.UserInfoEndpointDsl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@Qualifier("decorator")
public class OrderServiceProcessDecorator implements OrderService, ApplicationListener<OrderStatusUpdateEvent> {
    private final OrderService orderService;
    private final OrderProcessingService orderProcessingService;

    private final OrderNotifierService orderNotifierService;


    public OrderServiceProcessDecorator(@Qualifier("base_impl") OrderService orderService, OrderProcessingService orderProcessingService, OrderNotifierService orderNotifierService) {
        this.orderService = orderService;
        this.orderProcessingService = orderProcessingService;
        this.orderNotifierService = orderNotifierService;
    }

    @Override
    public boolean tryCreateOrder(StringBuffer message, Long userId) {
        boolean successStatus = orderService.tryCreateOrder(message, userId);
        if (successStatus) {
            Order order = orderService.findOrderItemByUserId(userId).getOrder();
            orderProcessingService.addOrder(order, orderService.getProcessingTime(order.getId()));
        }
        return successStatus;
    }

    @Override
    public boolean tryAddToOrderOrder(StringBuffer message, Long userId) {
        long durationBeforeAdding = orderService.getProcessingTime(orderService.findOrderItemByUserId(userId).getOrder().getId());
        boolean successStatus = orderService.tryAddToOrderOrder(message, userId);
        if (successStatus) {
            Order currentOrder = orderService.findOrderItemByUserId(userId).getOrder();
            long durationAfterAdding = orderService.getProcessingTime(currentOrder.getId());
            orderProcessingService.addProcessingTime(durationAfterAdding - durationBeforeAdding, currentOrder.getId());
        }
        return successStatus;
    }

    @Override
    public OrderItem findOrderItemByUserId(Long userId) {
        return orderService.findOrderItemByUserId(userId);
    }


    @Override
    public long getProcessingTime(Long orderId) {
        return orderService.getProcessingTime(orderId);
    }

    @Override
    public void updateOrderStatus(Long orderId, OrderStatus orderStatus) {
        orderService.updateOrderStatus(orderId, orderStatus);
    }

    @Override
    public Optional<Order> cancelOrder(Long userId) {
        Optional<Order> optionalOrder = orderService.cancelOrder(userId);
        optionalOrder.ifPresent(order -> orderProcessingService.cancelOrder(order.getId()));
        return optionalOrder;
    }

    @Override
    public void changeOrderStatus(Long userId, OrderStatus prevStatus, OrderStatus currentStatus) {
        orderService.changeOrderStatus(userId, prevStatus, currentStatus);
    }

    @Override
    public List<OrderItem> findOrderHistory(Long userId) {
        return orderService.findOrderHistory(userId);
    }


    @Override
    public void onApplicationEvent(OrderStatusUpdateEvent event) {
        updateOrderStatus(event.getOrderId(), event.getOrderStatus());
        orderNotifierService.sendOrderStatusUpdate(event.getOrderStatus(), event.getUserId());
    }

}

