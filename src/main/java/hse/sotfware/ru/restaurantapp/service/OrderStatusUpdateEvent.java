package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class OrderStatusUpdateEvent extends ApplicationEvent {

    private final OrderStatus orderStatus;
    private final Long userId;
    private final Long orderId;


    public OrderStatusUpdateEvent(Object source, OrderStatus orderStatus, Long userId, Long orderId) {
        super(source);
        this.orderStatus = orderStatus;
        this.userId = userId;
        this.orderId = orderId;
    }
}
