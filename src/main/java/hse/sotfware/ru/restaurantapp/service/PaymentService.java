package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.CartItem;
import hse.sotfware.ru.restaurantapp.model.OrderItem;
import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PaymentService {
    int calculateTotalPrice(OrderItem orderItem);

    int calculateTotalPrice(List<CartItem> orderItem);

    void payForOrder(Long userId);
}
