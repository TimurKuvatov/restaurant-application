package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.CartItem;
import hse.sotfware.ru.restaurantapp.model.OrderItem;
import hse.sotfware.ru.restaurantapp.model.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final OrderService orderService;

    public PaymentServiceImpl(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public int calculateTotalPrice(OrderItem orderItem) {
        if (orderItem == null || orderItem.getCartItems().isEmpty()) {
            return 0;
        }
        return calculateTotalPrice(orderItem.getCartItems());
    }

    @Override
    public int calculateTotalPrice(List<CartItem> cartItems) {
        return cartItems.stream().mapToInt(cartItem -> cartItem.getCount() * cartItem.getDish().getPrice()).sum();
    }

    @Override
    public void payForOrder(Long userId) {
        orderService.changeOrderStatus(userId, OrderStatus.COMPLETED, OrderStatus.PAID_FOR);
    }
}
