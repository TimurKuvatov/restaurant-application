package hse.sotfware.ru.restaurantapp.service;

import hse.sotfware.ru.restaurantapp.model.User;
import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;

public interface UserService {
    void save(UserRegistrationDetails userRegistrationDetails);

    User findByName(String username);
}