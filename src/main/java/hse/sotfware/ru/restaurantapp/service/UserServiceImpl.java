package hse.sotfware.ru.restaurantapp.service;


import hse.sotfware.ru.restaurantapp.model.User;
import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import hse.sotfware.ru.restaurantapp.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;


@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Override
    public void save(UserRegistrationDetails userRegistrationDetails) {
        User user = new User();
        user.setName(userRegistrationDetails.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationDetails.getPassword()));
        if (userRegistrationDetails.getRole() == null) {
            userRegistrationDetails.setRole("ROLE_USER");
        }
        user.setRoles(userRegistrationDetails.getRole());
        userRepository.save(user);
    }

    @Override
    public User findByName(String username) {
        Optional<User> optional = userRepository.findByName(username);
        return optional.orElse(null);
    }
}