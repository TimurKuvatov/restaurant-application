package hse.sotfware.ru.restaurantapp.validator;


import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;

@Component
public class AdminPasswordValidator {

    @Autowired
    private ApplicationArguments applicationArguments;

    @Value("${admin.secret.password}")
    private String adminSecretePassword;

    public Boolean validate(String password) {
        return password.equals(adminSecretePassword);
    }
}
