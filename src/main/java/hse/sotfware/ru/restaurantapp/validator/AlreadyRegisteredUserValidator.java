package hse.sotfware.ru.restaurantapp.validator;

import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import hse.sotfware.ru.restaurantapp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
public class AlreadyRegisteredUserValidator extends BaseUserValidator {

    public AlreadyRegisteredUserValidator() {
        super.errorMessage = "Пользователь с таким именем уже существует";
    }

    @Override
    protected Boolean checkCondition(UserRegistrationDetails userRegistrationDetails, StringBuffer message) {
        return userService.findByName(userRegistrationDetails.getUsername()) != null;
    }
}