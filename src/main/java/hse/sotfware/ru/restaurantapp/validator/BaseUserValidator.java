package hse.sotfware.ru.restaurantapp.validator;

import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;
import hse.sotfware.ru.restaurantapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseUserValidator implements UserValidator {

    @Autowired
    protected UserService userService;

    protected UserValidator nextValidator;

    protected String errorMessage;

    protected abstract Boolean checkCondition(UserRegistrationDetails userRegistrationDetails, StringBuffer message);

    public void setNext(UserValidator userValidator) {
        nextValidator = userValidator;
    }

    public Boolean validate(UserRegistrationDetails userRegistrationDetails, StringBuffer message) {
        if (checkCondition(userRegistrationDetails, message)) {
            message.append(errorMessage);
            return false;
        }
        if (nextValidator == null) {
            return true;
        }
        return nextValidator.validate(userRegistrationDetails, message);
    }


}
