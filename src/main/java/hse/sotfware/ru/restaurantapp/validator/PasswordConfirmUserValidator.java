package hse.sotfware.ru.restaurantapp.validator;

import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;

public class PasswordConfirmUserValidator extends BaseUserValidator {
    public PasswordConfirmUserValidator() {
        super.errorMessage = "Пароли должны совпадать";
    }

    @Override
    protected Boolean checkCondition(UserRegistrationDetails userRegistrationDetails, StringBuffer message) {
        return !userRegistrationDetails.getPasswordConfirmation().equals(userRegistrationDetails.getPassword());
    }
}
