package hse.sotfware.ru.restaurantapp.validator;

import hse.sotfware.ru.restaurantapp.model.UserRegistrationDetails;

public interface UserValidator {
    void setNext(UserValidator userValidator);

    Boolean validate(UserRegistrationDetails userRegistrationDetails, StringBuffer message);
}

